import csv
from django.core.management.base import BaseCommand
from search_address.settings import CSV_FILE_TO_PARSE
from address.models import Address


class Command(BaseCommand):
    help = 'Parse data from csv file to db'

    def handle(self, *args, **options):
        with open(CSV_FILE_TO_PARSE, encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for line in reader:
                address = Address(name=line['address'], lon=line['geo_lon'], lat=line['geo_lat'])
                address.save()
