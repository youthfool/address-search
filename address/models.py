from django.db import models
from django_admin_geomap import GeoItem


class Address(models.Model, GeoItem):
    """
    Address for points on the map
    """
    name = models.CharField(max_length=100)
    lon = models.FloatField()  # longitude
    lat = models.FloatField()  # latitude

    @property
    def geomap_longitude(self):
        return '' if self.lon is None else str(self.lon)

    @property
    def geomap_latitude(self):
        return '' if self.lon is None else str(self.lat)

    @property
    def geomap_popup_common(self):
        return "<strong>{}</strong>".format(str(self))

    @property
    def geomap_icon(self):
        return self.default_icon

    class Meta:
        verbose_name = 'address'
        verbose_name_plural = 'addresses'
        db_table = 'address'

    def __str__(self):
        return '{0}'.format(self.name)
