from dadata import Dadata
from django.shortcuts import render
from django_admin_geomap import geomap_context
from .models import Address
from search_address.settings import DADATA_API_KEY, DADATA_SECRET, ROOT_POINT_ICON
from geopy.distance import geodesic


def home(request):
    """
    Search address page is returned with markers
    :param request:
    :return:
    """
    search_address = request.GET.get('address', default='')
    search_range = request.GET.get('range', default='')

    address_list = []
    map_lon = 0.0
    map_lat = 0.0
    map_zoom = 10

    if len(search_address) != 0:
        dadata = Dadata(DADATA_API_KEY, DADATA_SECRET)
        result = dadata.clean("address", search_address)
        map_lon = float(result['geo_lon'])
        map_lat = float(result['geo_lat'])
        map_address = Address(name=search_address, lat=result['geo_lat'], lon=result['geo_lon'])
        map_address.default_icon = ROOT_POINT_ICON
        address_list.append(map_address)

        if len(search_range) != 0:
            try:
                search_range = float(search_range)
            except ValueError:
                pass
            else:
                for address in Address.objects.all():
                    distance = geodesic((map_lat, map_lon), (address.lat, address.lon)).km
                    if distance < search_range:
                        address_list.append(address)
                # stupid zoom
                if search_range > 900:
                    map_zoom = 2
                elif 900 >= search_range > 800:
                    map_zoom = 3
                elif 800 >= search_range > 600:
                    map_zoom = 4
                elif 600 >= search_range > 300:
                    map_zoom = 5
                elif 300 >= search_range > 150:
                    map_zoom = 6
                elif 150 >= search_range > 50:
                    map_zoom = 7
                else:
                    map_zoom = 9

    context = geomap_context(address_list, map_longitude=map_lon, map_latitude=map_lat, map_zoom=map_zoom)
    context['address_value'] = search_address
    context['range_value'] = search_range
    return render(request, 'home.html', context)
